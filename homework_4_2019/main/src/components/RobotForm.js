import React, {Component} from 'react';


class RobotForm extends Component
{
    constructor(props)
    {
        super(props)
        this.state = {
            name : '',
            type : '',
            mass : ''
        }
        
        
        
        this.add=(evt)=>
        {
            this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })
        }
        
        this.handleChange=(evt)=>
        {
            this.setState({
                [evt.target.name]:evt.target.value,
                
            })
        }
        
        
    
}
        


        render()
        {
             return <div>
            <input id="name" type="text" placeholder="name"  onChange={this.handleChange} name="name"/>
            <input id="type" type="text" placeholder="type"  onChange={this.handleChange} name="type"/>
             <input id="mass" type="text" placeholder="mass"  onChange={this.handleChange} name="mass"/>
            <input type="button" value="add" onClick={this.add} />
        </div>
        }
}

export default RobotForm